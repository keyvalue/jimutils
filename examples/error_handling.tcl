#!/usr/bin/env jimsh

proc faily {} {
    puts "executing fail"
    # raise an error
    #error "bad thing" [info stacktrace] 
    error "bad thing" x
    puts "never executed"
}

proc call_fail {} {
    faily
}

proc run {} {

    if {[catch { call_fail } msg opts]} {
        puts "fail"
        puts "msg: $msg"
        puts "opts: $opts"
        return
    }
    puts "success"
}

run
