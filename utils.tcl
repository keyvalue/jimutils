#!/usr/bin/env jimsh

proc handler_send { args } {
    set f [socket unix /home/coleman/.config/handler/ipc.sock]
    $f puts $args
    set resp [$f read]
    return $resp
}
