
# returns a date of the form:
# 2018-07-27T22:25:31,970072517-07:00
proc current_time {} {
    return [exec date --iso-8601=ns]
}

proc timed_cmd { args } {
    return [lindex [split [time {exec >@stdout {*}$args}]] 0]
}

proc todays_date {} {
    return [exec date --iso-8601=date]
}
