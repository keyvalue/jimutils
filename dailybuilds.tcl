#!/usr/bin/env jimsh

package require sled
package require chronox

sled db /home/coleman/.config/dailybuilds/data.db

set job_start [current_time]
puts "GLOBAL JOB START: $job_start"

# on success
proc run_job { name working_dir args } {
    global job_start
    variable duration
    cd $working_dir
    set now [current_time]
    puts "starting job $name, args: $args"
    puts "job start: $job_start"
    db put jobs!$job_start!$name!start $now
    if {[catch {set duration [timed_cmd $args]} msg opts]} {
        puts "FAILING"
        db put jobs!$job_start!$name!failed_at [current_time]
        db put jobs!$job_start!$name!status "error"
        return
    }
    db put jobs!$job_start!$name!duration_micros $duration
    db put jobs!$job_start!$name!status "success"
    puts "build done"
}

run_job alwaysfail [pwd] ./alwaysfail.sh
run_job alwayssucceed [pwd] ./alwayssucceed.sh
run_job rustup "/home/coleman" rustup update
run_job fitbit-syncd "/home/coleman/Code/Rust/fitbit-syncd" cargo build
run_job rocket-yew-starter-pack "/home/coleman/Code/Rust/rocket-yew-starter-pack" ./build.sh

puts "all builds done"
