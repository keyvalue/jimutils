#!/bin/bash

if [[ !  -z  $param  ]]; then
    echo "usage:"
    echo "    ./addlib.sh somelib.tcl"
    echo ""
fi

sudo ln -s "$(pwd)/${1}" /usr/local/lib/jim/${1}

