#!/usr/bin/env jimsh

package require sled

set rows 0

sled db /home/coleman/.config/dailybuilds/data.db

puts "found $rows rows"

db scan jobs { k v } {
    puts "key: $k value: $v"
}

